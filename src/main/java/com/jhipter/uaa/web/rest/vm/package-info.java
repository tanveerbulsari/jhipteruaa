/**
 * View Models used by Spring MVC REST controllers.
 */
package com.jhipter.uaa.web.rest.vm;
