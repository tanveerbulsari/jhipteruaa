package com.jhipter.uaa.config;

import java.util.Map;
import java.util.Optional;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.OAuth2RequestFactory;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.token.AbstractTokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;

import com.jhipter.uaa.domain.User;
import com.jhipter.uaa.repository.UserRepository;

public class CustomResourceOwnerPasswordTokenGranter extends AbstractTokenGranter {

	private UserRepository userRepository;
	private AuthenticationManager authenticationManager;

	CustomResourceOwnerPasswordTokenGranter(AuthorizationServerTokenServices tokenServices,
			ClientDetailsService clientDetailsService, OAuth2RequestFactory requestFactory, String grantType,
			UserRepository userRepository, AuthenticationManager authenticationManager) {
		super(tokenServices, clientDetailsService, requestFactory, grantType);
		this.userRepository = userRepository;
		this.authenticationManager = authenticationManager;
	}

	protected OAuth2Authentication getOAuth2Authentication(ClientDetails client, TokenRequest tokenRequest) {
		Map<String, String> params = tokenRequest.getRequestParameters();
		String username = params.get("username");
		String password = params.get("password");
		Authentication user = new UsernamePasswordAuthenticationToken(username, password);

		Authentication authentication = authenticationManager.authenticate(user);

		if (!authentication.isAuthenticated()) {
			throw new BadCredentialsException("Invalid credentials");
		}
		String signature = params.get("signature");
		String publicKey = params.get("publicKey");
		Optional<User> merchantUser = userRepository.findOneBySignatureAndPublicKey(signature, publicKey, username);
		if (merchantUser.isEmpty()) {
			throw new BadCredentialsException("Invalid signature/publicKey");
		}
		OAuth2Authentication oAuth2Authentication = new OAuth2Authentication(tokenRequest.createOAuth2Request(client),
				user);
		return oAuth2Authentication;
	}
}
