package com.jhipter.uaa.config;

import java.security.KeyPair;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.security.oauth2.provider.CompositeTokenGranter;
import org.springframework.security.oauth2.provider.TokenGranter;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.filter.CorsFilter;

import com.jhipter.uaa.repository.UserRepository;
import com.jhipter.uaa.security.AuthoritiesConstants;

import io.github.jhipster.config.JHipsterProperties;

@Configuration
@EnableAuthorizationServer
public class UaaConfiguration extends AuthorizationServerConfigurerAdapter implements ApplicationContextAware {
	/**
	 * Access tokens will not expire any earlier than this.
	 */
	private static final int MIN_ACCESS_TOKEN_VALIDITY_SECS = 60;

	@Autowired
	private ApplicationContext applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	@EnableResourceServer
	public static class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

		private final TokenStore tokenStore;

		private final JHipsterProperties jHipsterProperties;

		private final CorsFilter corsFilter;

		public ResourceServerConfiguration(TokenStore tokenStore, JHipsterProperties jHipsterProperties,
				CorsFilter corsFilter) {
			this.tokenStore = tokenStore;
			this.jHipsterProperties = jHipsterProperties;
			this.corsFilter = corsFilter;
		}

		@Override
		public void configure(HttpSecurity http) throws Exception {
			http.exceptionHandling()
					.authenticationEntryPoint((request, response, authException) -> response
							.sendError(HttpServletResponse.SC_UNAUTHORIZED))
					.and().csrf().disable().addFilterBefore(corsFilter, UsernamePasswordAuthenticationFilter.class)
					.headers().frameOptions().disable().and().sessionManagement()
					.sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests()
					.antMatchers("/api/register").permitAll().antMatchers("/api/activate").permitAll()
					.antMatchers("/api/authenticate").permitAll().antMatchers("/api/account/reset-password/init")
					.permitAll().antMatchers("/api/account/reset-password/finish").permitAll().antMatchers("/api/**")
					.authenticated().antMatchers("/management/health").permitAll().antMatchers("/management/**")
					.hasAuthority(AuthoritiesConstants.ADMIN).antMatchers("/v2/api-docs/**").permitAll()
					.antMatchers("/swagger-resources/configuration/ui").permitAll()
					.antMatchers("/swagger-ui/index.html").hasAuthority(AuthoritiesConstants.ADMIN);
		}

		@Override
		public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
			resources.resourceId("jhipster-uaa").tokenStore(tokenStore);
		}
	}

	private final JHipsterProperties jHipsterProperties;

	private final UaaProperties uaaProperties;

	private final PasswordEncoder passwordEncoder;

	public UaaConfiguration(JHipsterProperties jHipsterProperties, UaaProperties uaaProperties,
			PasswordEncoder passwordEncoder) {
		this.jHipsterProperties = jHipsterProperties;
		this.uaaProperties = uaaProperties;
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		int accessTokenValidity = uaaProperties.getWebClientConfiguration().getAccessTokenValidityInSeconds();
		accessTokenValidity = Math.max(accessTokenValidity, MIN_ACCESS_TOKEN_VALIDITY_SECS);
		int refreshTokenValidity = uaaProperties.getWebClientConfiguration()
				.getRefreshTokenValidityInSecondsForRememberMe();
		refreshTokenValidity = Math.max(refreshTokenValidity, accessTokenValidity);
		/*
		 * For a better client design, this should be done by a ClientDetailsService
		 * (similar to UserDetailsService).
		 */
		clients.inMemory().withClient(uaaProperties.getWebClientConfiguration().getClientId())
				.secret(passwordEncoder.encode(uaaProperties.getWebClientConfiguration().getSecret())).scopes("openid")
				.autoApprove(true)
				.authorizedGrantTypes("implicit", "refresh_token", "password", "authorization_code", "custom-password")
				.accessTokenValiditySeconds(accessTokenValidity).refreshTokenValiditySeconds(refreshTokenValidity).and()
				.withClient(jHipsterProperties.getSecurity().getClientAuthorization().getClientId())
				.secret(passwordEncoder
						.encode(jHipsterProperties.getSecurity().getClientAuthorization().getClientSecret()))
				.scopes("web-app").authorities("ROLE_ADMIN").autoApprove(true)
				.authorizedGrantTypes("client_credentials", "custom-password")
				.accessTokenValiditySeconds(
						(int) jHipsterProperties.getSecurity().getAuthentication().getJwt().getTokenValidityInSeconds())
				.refreshTokenValiditySeconds((int) jHipsterProperties.getSecurity().getAuthentication().getJwt()
						.getTokenValidityInSecondsForRememberMe());
	}

	@Bean
	public ClientDetailsService clientDetailsService() {
		return new ClientDetailsService() {
			@Override
			public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
				BaseClientDetails details = new BaseClientDetails();
				details.setClientId(clientId);
				details.setClientSecret(passwordEncoder.encode(uaaProperties.getWebClientConfiguration().getSecret()));
				details.setAuthorizedGrantTypes(Arrays.asList("implicit", "refresh_token", "password",
						"authorization_code", "custom-password"));
				details.setScope(Arrays.asList("openid"));
				details.setResourceIds(Arrays.asList("oauth2-resource"));
				details.setAccessTokenValiditySeconds((int) jHipsterProperties.getSecurity().getAuthentication()
						.getJwt().getTokenValidityInSeconds());
				Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
				authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
				details.setAuthorities(authorities);
				return details;
			}
		};
	} // */

	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		// pick up all TokenEnhancers incl. those defined in the application
		// this avoids changes to this class if an application wants to add its own to
		// the chain
		Collection<TokenEnhancer> tokenEnhancers = applicationContext.getBeansOfType(TokenEnhancer.class).values();
		TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
		tokenEnhancerChain.setTokenEnhancers(new ArrayList<>(tokenEnhancers));
		endpoints.tokenGranter(tokenGranter(endpoints));
		endpoints.authenticationManager(authenticationManager).tokenStore(tokenStore())
				.tokenEnhancer(tokenEnhancerChain).reuseRefreshTokens(false); // don't
																				// reuse or
																				// we will
																				// run into
																				// session
																				// inactivity
																				// timeouts
	}

	@Autowired
	@Qualifier("authenticationManagerBean")
	private AuthenticationManager authenticationManager;

	/**
	 * Apply the token converter (and enhancer) for token store.
	 * 
	 * @return the {@link JwtTokenStore} managing the tokens.
	 */
	@Bean
	public JwtTokenStore tokenStore() {
		return new JwtTokenStore(jwtAccessTokenConverter());
	}

	/**
	 * This bean generates an token enhancer, which manages the exchange between JWT
	 * access tokens and Authentication in both directions.
	 *
	 * @return an access token converter configured with the authorization server's
	 *         public/private keys.
	 */
	@Bean
	public JwtAccessTokenConverter jwtAccessTokenConverter() {
		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
		KeyPair keyPair = new KeyStoreKeyFactory(new ClassPathResource(uaaProperties.getKeyStore().getName()),
				uaaProperties.getKeyStore().getPassword().toCharArray())
						.getKeyPair(uaaProperties.getKeyStore().getAlias());
		converter.setKeyPair(keyPair);
		return converter;
	}

	@Override
	public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
		oauthServer.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()");
	}

	@Bean
	public DefaultOAuth2RequestFactory defaultOAuth2RequestFactory() {
		return new DefaultOAuth2RequestFactory(clientDetailsService());

	}

	@Autowired
	private UserRepository userRepository;

	private TokenGranter tokenGranter(final AuthorizationServerEndpointsConfigurer endpoints) {
		List<TokenGranter> granters = new ArrayList<TokenGranter>(Arrays.asList(endpoints.getTokenGranter()));
		granters.add(new CustomResourceOwnerPasswordTokenGranter(endpoints.getTokenServices(),
				endpoints.getClientDetailsService(), endpoints.getOAuth2RequestFactory(), "custom-password",
				userRepository, authenticationManager));
		return new CompositeTokenGranter(granters);
	}

}
